/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rockola_music.rockola_music_backend.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Toonami
 */
@Entity
@Table(name = "canciones")

public class Canciones implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_cancion")
    private Integer id_cancion;
 
    @Column(name = "nombrecancion")
    private String nombreCancion;
    @Column(name = "album")
    private String album;
    @Column(name = "año")
    private Integer año;
    @Column(name = "genero")
    private String Genero;
    @Column(name = "interprete")
    private String Interprete;
    @Column(name = "vocalista")
    private String Vocalista;
    @Column(name = "pais")
    private String Pais;
    
    
    
    @ManyToOne
    @JoinColumn(name = "id_refletra")
    private Letra letra;
    
    @ManyToOne
    @JoinColumn(name = "id_reflista")
    private Lista lista;
    
    @ManyToOne
    @JoinColumn(name = "id_refadministrador")
    private Administrador administrador;
    

    public Integer getId_cancion() {
        return id_cancion;
    }

    public void setId_cancion(Integer id_cancion) {
        this.id_cancion = id_cancion;
    }
    
    

    public String getNombreCancion() {
        return nombreCancion;
    }

    public void setNombreCancion(String nombreCancion) {
        this.nombreCancion = nombreCancion;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public Integer getAño() {
        return año;
    }

    public void setAño(Integer año) {
        this.año = año;
    }

    public String getGenero() {
        return Genero;
    }

    public void setGenero(String Genero) {
        this.Genero = Genero;
    }

    public String getInterprete() {
        return Interprete;
    }

    public void setInterprete(String Interprete) {
        this.Interprete = Interprete;
    }

    public String getVocalista() {
        return Vocalista;
    }

    public void setVocalista(String Vocalista) {
        this.Vocalista = Vocalista;
    }

    public String getPais() {
        return Pais;
    }

    public void setPais(String Pais) {
        this.Pais = Pais;
    }

    public Letra getLetra() {
        return letra;
    }

    public void setLetra(Letra letra) {
        this.letra = letra;
    }

    public Lista getLista() {
        return lista;
    }

    public void setLista(Lista lista) {
        this.lista = lista;
    }

    public Administrador getAdministrador() {
        return administrador;
    }

    public void setAdministrador(Administrador administrador) {
        this.administrador = administrador;
    }
    
    
    
    
}
