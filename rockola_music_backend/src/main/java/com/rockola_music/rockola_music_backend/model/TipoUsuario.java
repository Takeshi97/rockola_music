/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rockola_music.rockola_music_backend.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Toonami
 */
@Entity
@Table(name = "tipousuario")

public class TipoUsuario implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_tipousuario")
    private Integer id_tipoUsuario;
    @Column(name = "rol")
    private String Rol;
    @Column(name = "edad")
    private Integer Edad;

    public Integer getId_tipoUsuario() {
        return id_tipoUsuario;
    }

    public void setId_tipoUsuario(Integer id_tipoUsuario) {
        this.id_tipoUsuario = id_tipoUsuario;
    }

    public String getRol() {
        return Rol;
    }

    public void setRol(String Rol) {
        this.Rol = Rol;
    }

    public Integer getEdad() {
        return Edad;
    }

    public void setEdad(Integer Edad) {
        this.Edad = Edad;
    }
    
    
}
