/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rockola_music.rockola_music_backend.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.hibernate.annotations.GenericGenerator;

/**
 *
 * @author Toonami
 */

@Entity
@Table(name = "letra")

public class Letra implements Serializable {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_letra")
    private Integer id_letra;
    @Column(name = "letracancion")
    private String LetraCancion;

    public Integer getId_letra() {
        return id_letra;
    }

    public void setId_letra(Integer id_letra) {
        this.id_letra = id_letra;
    }

    public String getLetraCancion() {
        return LetraCancion;
    }

    public void setLetraCancion(String LetraCancion) {
        this.LetraCancion = LetraCancion;
    }
    
    
}
