/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rockola_music.rockola_music_backend.controller;

import com.rockola_music.rockola_music_backend.model.Canciones;
import com.rockola_music.rockola_music_backend.service.CancionesService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Toonami
 */

@RestController
@CrossOrigin("*")
@RequestMapping("/canciones")

public class CancionesController {
    
    @Autowired
    private CancionesService cancionesservice;

    @PostMapping(value = "/")
    public ResponseEntity<Canciones> agregar(@RequestBody Canciones canciones) {
        Canciones obj = cancionesservice.save(canciones);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Canciones> eliminar(@PathVariable Integer id) {
        Canciones obj = cancionesservice.findById(id);
        if (obj != null) {
            cancionesservice.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<Canciones> editar(@RequestBody Canciones canciones) {
        Canciones obj = cancionesservice.findById(canciones.getId_cancion());
        if (obj != null) {
            
            obj.setNombreCancion(canciones.getNombreCancion());
            obj.setAlbum(canciones.getAlbum());
            obj.setAño(canciones.getAño());
            obj.setGenero(canciones.getGenero());
            obj.setInterprete(canciones.getInterprete());
            obj.setVocalista(canciones.getVocalista());
            obj.setPais(canciones.getPais());
            obj.setLetra(canciones.getLetra());
            obj.setLista(canciones.getLista());
            obj.setAdministrador(canciones.getAdministrador());
            
            cancionesservice.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<Canciones> consultarTodo() {
        return cancionesservice.findAll();
    }

    @GetMapping("/list/{id}")
    public Canciones consultaPorId(@PathVariable Integer id) {
        return cancionesservice.findById(id);
    }


}
