/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rockola_music.rockola_music_backend.controller;

import com.rockola_music.rockola_music_backend.model.Lista;
import com.rockola_music.rockola_music_backend.service.ListaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Toonami
 */

@RestController
@CrossOrigin("*")
@RequestMapping("/lista")

public class ListaController {
    
    @Autowired
    private ListaService listaservice;

    @PostMapping(value = "/")
    public ResponseEntity<Lista> agregar(@RequestBody Lista lista) {
        Lista obj = listaservice.save(lista);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Lista> eliminar(@PathVariable Integer id) {
        Lista obj = listaservice.findById(id);
        if (obj != null) {
            listaservice.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<Lista> editar(@RequestBody Lista lista) {
        Lista obj = listaservice.findById(lista.getId_lista());
        if (obj != null) {
            obj.setNombreLista(lista.getNombreLista());
            
            
            listaservice.save(obj);
            
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<Lista> consultarTodo() {
        return listaservice.findAll();
    }

    @GetMapping("/list/{id}")
    public Lista consultaPorId(@PathVariable Integer id) {
        return listaservice.findById(id);
    }
    
}
