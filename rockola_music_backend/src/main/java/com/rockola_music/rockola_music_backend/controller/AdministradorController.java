/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rockola_music.rockola_music_backend.controller;

import com.rockola_music.rockola_music_backend.model.Administrador;
import com.rockola_music.rockola_music_backend.service.AdministradorService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Toonami
 */

@RestController
@CrossOrigin("*")
@RequestMapping("/administrador")

public class AdministradorController {
    
    @Autowired
    private AdministradorService administradorservice;

    @PostMapping(value = "/")
    public ResponseEntity<Administrador> agregar(@RequestBody Administrador administrador) {
        Administrador obj = administradorservice.save(administrador);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Administrador> eliminar(@PathVariable Integer id) {
        Administrador obj = administradorservice.findById(id);
        if (obj != null) {
            administradorservice.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<Administrador> editar(@RequestBody Administrador administrador) {
        Administrador obj = administradorservice.findById(administrador.getId_admin());
        if (obj != null) {
            obj.setNombre(administrador.getNombre());
            obj.setApellido(administrador.getApellido());
            obj.setTelefono_Movil(administrador.getTelefono_Movil());
            obj.setUserName(administrador.getUserName());
            obj.setE_Mail(administrador.getE_Mail());
            obj.setPassword(administrador.getPassword());
            obj.setTipousuario(administrador.getTipousuario());
            
            administradorservice.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<Administrador> consultarTodo() {
        return administradorservice.findAll();
    }

    @GetMapping("/list/{id}")
    public Administrador consultaPorId(@PathVariable Integer id) {
        return administradorservice.findById(id);
    }
}
