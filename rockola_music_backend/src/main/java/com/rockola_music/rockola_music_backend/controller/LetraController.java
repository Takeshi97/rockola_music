/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rockola_music.rockola_music_backend.controller;

import com.rockola_music.rockola_music_backend.model.Letra;
import com.rockola_music.rockola_music_backend.service.LetraService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Toonami
 */

@RestController
@CrossOrigin("*")
@RequestMapping("/letra")

public class LetraController {
    
    @Autowired
    private LetraService letraservice;

    @PostMapping(value = "/")
    public ResponseEntity<Letra> agregar(@RequestBody Letra letra) {
        Letra obj = letraservice.save(letra);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Letra> eliminar(@PathVariable Integer id) {
        Letra obj = letraservice.findById(id);
        if (obj != null) {
            letraservice.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<Letra> editar(@RequestBody Letra letra) {
        Letra obj = letraservice.findById(letra.getId_letra());
        if (obj != null) {
            obj.setLetraCancion(letra.getLetraCancion());
            
            letraservice.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<Letra> consultarTodo() {
        return letraservice.findAll();
    }

    @GetMapping("/list/{id}")
    public Letra consultaPorId(@PathVariable Integer id) {
        return letraservice.findById(id);
    }
    
}
