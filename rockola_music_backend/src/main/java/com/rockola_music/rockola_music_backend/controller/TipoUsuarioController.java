/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rockola_music.rockola_music_backend.controller;

import com.rockola_music.rockola_music_backend.model.TipoUsuario;
import com.rockola_music.rockola_music_backend.service.TipoUsuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Toonami
 */

@RestController
@CrossOrigin("*")
@RequestMapping("/tipousuario")

public class TipoUsuarioController {
    
    @Autowired
    private TipoUsuarioService tipousuarioservice;

    @PostMapping(value = "/")
    public ResponseEntity<TipoUsuario> agregar(@RequestBody TipoUsuario tipousuario) {
        TipoUsuario obj = tipousuarioservice.save(tipousuario);
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<TipoUsuario> eliminar(@PathVariable Integer id) {
        TipoUsuario obj = tipousuarioservice.findById(id);
        if (obj != null) {
            tipousuarioservice.delete(id);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @PutMapping(value = "/")
    public ResponseEntity<TipoUsuario> editar(@RequestBody TipoUsuario tipousuario) {
        TipoUsuario obj = tipousuarioservice.findById(tipousuario.getId_tipoUsuario());
        if (obj != null) {
            obj.setRol(tipousuario.getRol());
            obj.setEdad(tipousuario.getEdad());
            
            tipousuarioservice.save(obj);
        } else {
            return new ResponseEntity<>(obj, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(obj, HttpStatus.OK);
    }

    @GetMapping("/list")
    public List<TipoUsuario> consultarTodo() {
        return tipousuarioservice.findAll();
    }

    @GetMapping("/list/{id}")
    public TipoUsuario consultaPorId(@PathVariable Integer id) {
        return tipousuarioservice.findById(id);
    }
}
