package com.rockola_music.rockola_music_backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RockolaMusicBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(RockolaMusicBackendApplication.class, args);
	}

}
