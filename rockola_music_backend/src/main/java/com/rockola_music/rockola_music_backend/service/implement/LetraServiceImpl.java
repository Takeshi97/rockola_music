/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rockola_music.rockola_music_backend.service.implement;

import com.rockola_music.rockola_music_backend.dao.LetraDao;
import com.rockola_music.rockola_music_backend.model.Letra;
import com.rockola_music.rockola_music_backend.service.LetraService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Toonami
 */

@Service
public class LetraServiceImpl implements LetraService {
    
    @Autowired
    private LetraDao letraDao;

    @Override
    @Transactional(readOnly = false)
    public Letra save(Letra letra) {
        return letraDao.save(letra);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        letraDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Letra findById(Integer id) {
        return letraDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Letra> findAll() {
        return (List<Letra>) letraDao.findAll();
    }
}
