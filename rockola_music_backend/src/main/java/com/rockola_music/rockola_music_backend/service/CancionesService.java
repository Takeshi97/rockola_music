/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rockola_music.rockola_music_backend.service;

import com.rockola_music.rockola_music_backend.model.Canciones;
import java.util.List;

/**
 *
 * @author Toonami
 */

public interface CancionesService {
    
    public Canciones save(Canciones canciones);
    public void delete(Integer id);
    public Canciones findById(Integer id);
    public List<Canciones> findAll();
}
