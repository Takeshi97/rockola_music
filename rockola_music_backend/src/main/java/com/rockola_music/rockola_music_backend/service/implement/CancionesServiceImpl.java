/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rockola_music.rockola_music_backend.service.implement;

import com.rockola_music.rockola_music_backend.dao.CancionesDao;
import com.rockola_music.rockola_music_backend.model.Canciones;
import com.rockola_music.rockola_music_backend.service.CancionesService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Toonami
 */


@Service
public class CancionesServiceImpl implements CancionesService {
    
    @Autowired
    private CancionesDao cancionesDao;

    @Override
    @Transactional(readOnly = false)
    public Canciones save(Canciones canciones) {
        return cancionesDao.save(canciones);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        cancionesDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Canciones findById(Integer id) {
        return cancionesDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Canciones> findAll() {
        return (List<Canciones>) cancionesDao.findAll();
    }
    
}
