/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rockola_music.rockola_music_backend.service.implement;

import com.rockola_music.rockola_music_backend.dao.ListaDao;
import com.rockola_music.rockola_music_backend.model.Lista;
import com.rockola_music.rockola_music_backend.service.ListaService;
import java.util.List; /**/
//import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired; /**/
import org.springframework.stereotype.Service; /**/
import org.springframework.transaction.annotation.Transactional;



/**
 *
 * @author Toonami
 */

@Service
public class ListaServiceImpl implements ListaService {
    
    @Autowired
    private ListaDao listaDao;

    @Override
    @Transactional(readOnly = false)
    public Lista save(Lista lista) {
        return listaDao.save(lista);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        listaDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public Lista findById(Integer id) {
        return listaDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Lista> findAll() {
        return (List<Lista>) listaDao.findAll();
    }
}
