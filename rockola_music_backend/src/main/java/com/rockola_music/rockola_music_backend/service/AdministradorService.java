/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rockola_music.rockola_music_backend.service;

import com.rockola_music.rockola_music_backend.model.Administrador;
import java.util.List;

/**
 *
 * @author Toonami
 */
public interface AdministradorService {
    
    public Administrador save(Administrador administrador);
    public void delete(Integer id);
    public Administrador findById(Integer id);
    public List<Administrador> findAll();
}
