/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rockola_music.rockola_music_backend.service.implement;

import com.rockola_music.rockola_music_backend.dao.TipoUsuarioDao;
import com.rockola_music.rockola_music_backend.model.TipoUsuario;
import com.rockola_music.rockola_music_backend.service.TipoUsuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Toonami
 */

@Service
public class TipoUsuarioServiceImpl implements TipoUsuarioService {
    
    @Autowired
    private TipoUsuarioDao tipousuarioDao;

    @Override
    @Transactional(readOnly = false)
    public TipoUsuario save(TipoUsuario tipousuario) {
        return tipousuarioDao.save(tipousuario);
    }

    @Override
    @Transactional(readOnly = false)
    public void delete(Integer id) {
        tipousuarioDao.deleteById(id);
    }

    @Override
    @Transactional(readOnly = true)
    public TipoUsuario findById(Integer id) {
        return tipousuarioDao.findById(id).orElse(null);
    }

    @Override
    @Transactional(readOnly = true)
    public List<TipoUsuario> findAll() {
        return (List<TipoUsuario>) tipousuarioDao.findAll();
    }
}
