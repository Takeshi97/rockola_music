/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rockola_music.rockola_music_backend.service;

import com.rockola_music.rockola_music_backend.model.Letra;
import java.util.List;

/**
 *
 * @author Toonami
 */

public interface LetraService {
    
    public Letra save(Letra letra);
    public void delete(Integer id);
    public Letra findById(Integer id);
    public List<Letra> findAll();
}
