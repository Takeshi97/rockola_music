/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rockola_music.rockola_music_backend.service;

import com.rockola_music.rockola_music_backend.model.Lista;
import java.util.List;


/**
 *
 * @author Toonami
 */
public interface ListaService {
    
    public Lista save(Lista lista);
    public void delete(Integer id);
    public Lista findById(Integer id);
    public List<Lista> findAll();
}
