/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rockola_music.rockola_music_backend.service;

import com.rockola_music.rockola_music_backend.model.TipoUsuario;
import java.util.List;

/**
 *
 * @author Toonami
 */
public interface TipoUsuarioService {
    
    public TipoUsuario save(TipoUsuario tipousuario);
    public void delete(Integer id);
    public TipoUsuario findById(Integer id);
    public List<TipoUsuario> findAll();
}
