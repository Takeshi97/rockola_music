/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.rockola_music.rockola_music_backend.dao;

import com.rockola_music.rockola_music_backend.model.Canciones;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Toonami
 */

public interface CancionesDao extends CrudRepository<Canciones,Integer> {
    
    
}
