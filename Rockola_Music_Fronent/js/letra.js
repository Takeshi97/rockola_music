
function loadData(){
    let request = sendRequest('letra/list', 'GET', '')
    let table = document.getElementById('letra-table');
    table.innerHTML = "";
    request.onload = function(){
        
        let data = request.response;
        console.log(data);
        data.forEach((element, index) => {
            table.innerHTML += `
                <tr>
                    <th>${element.id_letra}</th>
                    <td>${element.letraCancion}</td>
                    
                    <td>
                        <button type="button" class="btn btn-primary" onclick='window.location = "/form_letra.html?id=${element.id_letra}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='deleteLetra(${element.id_letra})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="6">Error al recuperar los datos.</td>
            </tr>
        `;
    }
}

function loadLetra(id_letra){
    let request = sendRequest('letra/list/'+id_letra, 'GET', '')
    let letra = document.getElementById('cancion-letra')
    
    
    let id = document.getElementById('letra-id')
    request.onload = function(){
        
        let data = request.response
        id.value = data.id_letra
        letra.value = data.letraCancion
        
        
    }
    request.onerror = function(){
        alert("Error al recuperar los datos.");
    }
}

function deleteLetra(id_letra){
    let request = sendRequest('letra/'+id_letra, 'DELETE', '')
    request.onload = function(){
        loadData()
    }
}

function saveLetra(){
    let letra = document.getElementById('cancion-letra').value
    
    
    let id = document.getElementById('letra-id').value
    let data = {'id_letra': id,'letraCancion': letra }
    let request = sendRequest('letra/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'letra.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios.')
    }
}