
function loadData(){
    let request = sendRequest('lista/list', 'GET', '')
    let table = document.getElementById('list-table');
    table.innerHTML = "";
    request.onload = function(){
        
        let data = request.response;
        console.log(data);
        data.forEach((element, index) => {
            table.innerHTML += `
                <tr>
                    <th>${element.id_lista}</th>
                    <td>${element.nombreLista}</td>
                    
                    <td>
                        <button type="button" class="btn btn-primary" onclick='window.location = "/form_lista.html?id=${element.id_lista}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='deleteLista(${element.id_lista})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="6">Error al recuperar los datos.</td>
            </tr>
        `;
    }
}

function loadLista(id_lista){
    let request = sendRequest('lista/list/'+id_lista, 'GET', '')
    let lista = document.getElementById('name-lista')
    
    
    let id = document.getElementById('lista-id')
    request.onload = function(){
        
        let data = request.response
        id.value = data.id_lista
        lista.value = data.nombreLista
        
        
    }
    request.onerror = function(){
        alert("Error al recuperar los datos.");
    }
}

function deleteLista(id_lista){
    let request = sendRequest('lista/'+id_lista, 'DELETE', '')
    request.onload = function(){
        loadData()
    }
}

function saveLista(){
    let lista = document.getElementById('name-lista').value
    
    
    let id = document.getElementById('lista-id').value
    let data = {'id_lista': id,'nombreLista': lista }
    let request = sendRequest('lista/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'lista.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios.')
    }
}