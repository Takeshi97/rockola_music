
function loadData(){
    let request = sendRequest('administrador/list', 'GET', '')
    let table = document.getElementById('users-table');
    table.innerHTML = "";
    request.onload = function(){
        
        let data = request.response;
        console.log(data);
        data.forEach((element, index) => {
            table.innerHTML += `
                <tr>
                    <th>${element.id_admin}</th>
                    <td>${element.nombre}</td>
                    <td>${element.apellido}</td>
                    <td>${element.telefono_Movil}</td>
                    <td>${element.userName}</td>
                    <td>${element.e_Mail}</td>
                    <td>${element.password}</td>
                    <td>
                        <button type="button" class="btn btn-primary" onclick='window.location = "/form_administrador.html?id=${element.id_admin}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='deleteAdmin(${element.id_admin})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="6">Error al recuperar los datos.</td>
            </tr>
        `;
    }
}

function loadAdmin(id_admin){
    let request = sendRequest('administrador/list/'+id_admin, 'GET', '')
    let name = document.getElementById('admin-name')
    let apellido = document.getElementById('admin-apellido')
    let telefono_movil = document.getElementById('admin-telefono_movil')
    let username = document.getElementById('admin-username')
    let e_mail = document.getElementById('admin-e_mail')
    let password = document.getElementById('admin-password')
    
    let id = document.getElementById('admin-id')
    request.onload = function(){
        
        let data = request.response
        id.value = data.id_admin
        name.value = data.nombre
        apellido.value = data.apellido
        telefono_movil.value = data.telefono_Movil
        username.value = data.userName
        e_mail.value = data.e_Mail
        password.value = data.password
        
    }
    request.onerror = function(){
        alert("Error al recuperar los datos.");
    }
}

function deleteAdmin(id_admin){
    let request = sendRequest('administrador/'+id_admin, 'DELETE', '')
    request.onload = function(){
        loadData()
    }
}

function saveAdmin(){
    let name = document.getElementById('admin-name').value
    let apellido = document.getElementById('admin-apellido').value
    let telefono_movil = document.getElementById('admin-telefono_movil').value
    let username = document.getElementById('admin-username').value
    let e_mail = document.getElementById('admin-e_mail').value
    let password = document.getElementById('admin-password').value
    
    
    let id = document.getElementById('admin-id').value
    let data = {'id_admin': id,'nombre':name,'apellido': apellido, 'telefono_Movil': telefono_movil, 'userName':username, 'e_Mail':e_mail, 'password':password }
    let request = sendRequest('administrador/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'administrador.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios.')
    }
}