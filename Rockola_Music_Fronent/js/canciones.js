
function loadData(){
    let request = sendRequest('canciones/list', 'GET', '')
    let table = document.getElementById('songs-table');
    table.innerHTML = "";
    request.onload = function(){
        
        let data = request.response;
        console.log(data);
        data.forEach((element, index) => {
            table.innerHTML += `
                <tr>
                    <th>${element.id_cancion}</th>
                    <td>${element.nombreCancion}</td>
                    <td>${element.album}</td>
                    <td>${element.año}</td>
                    <td>${element.genero}</td>
                    <td>${element.interprete}</td>
                    <td>${element.vocalista}</td>
                    <td>${element.pais}</td>
                    <td>
                        <button type="button" class="btn btn-primary" onclick='window.location = "/form_canciones.html?id=${element.id_cancion}"'>Editar</button>
                        <button type="button" class="btn btn-danger" onclick='deleteCanciones(${element.id_cancion})'>Eliminar</button>
                    </td>
                </tr>

                `
        });
    }
    request.onerror = function(){
        table.innerHTML = `
            <tr>
                <td colspan="6">Error al recuperar los datos.</td>
            </tr>
        `;
    }
}

function loadCanciones(id_cancion){
    let request = sendRequest('canciones/list/'+id_cancion, 'GET', '')
    let name = document.getElementById('cancion-name')
    let album = document.getElementById('cancion-album')
    let año = document.getElementById('cancion-año')
    let genero = document.getElementById('cancion-genero')
    let interprete = document.getElementById('cancion-interprete')
    let vocalista = document.getElementById('cancion-vocalista')
    let pais = document.getElementById('cancion-pais')
    
    let id = document.getElementById('cancion-id')
    request.onload = function(){
        
        let data = request.response
        id.value = data.id_cancion
        name.value = data.nombreCancion
        album.value = data.album
        año.value = data.año
        genero.value = data.genero
        interprete.value = data.interprete
        vocalista.value = data.vocalista
        pais.value = data.pais
    }
    request.onerror = function(){
        alert("Error al recuperar los datos.");
    }
}

function deleteCanciones(id_cancion){
    let request = sendRequest('canciones/'+id_cancion, 'DELETE', '')
    request.onload = function(){
        loadData()
    }
}

function saveCanciones(){
    let name = document.getElementById('cancion-name').value
    let album = document.getElementById('cancion-album').value
    let año = document.getElementById('cancion-año').value
    let genero = document.getElementById('cancion-genero').value
    let interprete = document.getElementById('cancion-interprete').value
    let vocalista = document.getElementById('cancion-vocalista').value
    let pais = document.getElementById('cancion-pais').value
    
    let id = document.getElementById('cancion-id').value
    let data = {'id_cancion': id,'nombreCancion':name,'album': album, 'año': año, 'genero':genero, 'interprete':interprete, 'vocalista':vocalista, 'pais':pais }
    let request = sendRequest('canciones/', id ? 'PUT' : 'POST', data)
    request.onload = function(){
        window.location = 'canciones.html';
    }
    request.onerror = function(){
        alert('Error al guardar los cambios.')
    }
}